package javadesign.ui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class TypeInfoFrame extends JFrame {
    Container contentPane;

    DefaultTableModel model;

    JTable tableMain;

    public TypeInfoFrame() throws HeadlessException {
        contentPane = this.getContentPane();

        this.setSize(400, 400);

        String[][] data = new String[][] {
                {"1", "A", "a"},
                {"2", "B", "b"},
                {"3", "C", "c"},
                {"4", "D", "d"},
        };

        String[] col = new String[] { "编号", "大类", "小类" };

        model = new DefaultTableModel(data, col);
        tableMain = new JTable(model);

        contentPane.add(tableMain);
    }
}
