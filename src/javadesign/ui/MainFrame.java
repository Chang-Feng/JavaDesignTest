package javadesign.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {
    Container contentPane;

    JMenuBar menuMain;

    JMenu menuSystemManagement;
    JMenu menuAssetsInfo;

    JMenuItem menuItemType;
    JMenuItem menuItemExit;

    ActionListener actionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "TYPE":
                    TypeInfoFrame typeInfoFrame = new TypeInfoFrame();
                    typeInfoFrame.setVisible(true);
                    break;
                case "EXIT":
                    System.exit(0);
                    break;
            }
        }
    };

    public MainFrame() throws HeadlessException {
        contentPane = this.getContentPane();

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 400);
        this.setTitle("管理系统");

        menuMain = new JMenuBar();
        this.setJMenuBar(menuMain);

        menuSystemManagement = new JMenu("系统管理");
        menuMain.add(menuSystemManagement);

        menuItemType = new JMenuItem("类别管理");
        menuItemType.setActionCommand("TYPE");
        menuItemType.addActionListener(actionListener);
        menuSystemManagement.add(menuItemType);

        menuItemExit = new JMenuItem("退出");
        menuItemExit.setActionCommand("EXIT");
        menuItemExit.addActionListener(actionListener);
        menuSystemManagement.add(menuItemExit);

        menuAssetsInfo = new JMenu("资产信息管理");
        menuMain.add(menuAssetsInfo);
    }
}
